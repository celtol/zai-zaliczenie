import React, { Component} from 'react';
import DatePickerStart from "react-datepicker";
import DatePickerEnd from "react-datepicker";

import "react-datepicker/dist/react-datepicker.css";
import './App.css';

var resultofcounting = []

function findMidCur(data){
  var temp1 = data
  var temp2 = data.length
  var temp3 = []
  var temp4 = 0
  for (var i = 0; i < temp2; i++) {
    
    for (var j = 0; j < temp1[i].rates.length; j++) {
      if (temp4 !== temp1[i].rates.length) {
        temp3[j] = temp1[i].rates[j]
        temp4 = temp4 + 1
      }else{
        temp3[j].mid = temp3[j].mid + temp1[i].rates[j].mid

      }
    }
  }
  for (var k = 0; k < temp2; k++) {
    temp3[k].mid = temp3[k].mid/temp2
  }
  resultofcounting = temp3
  findinborders()
}

function findinborders(){
  var temp11 = document.getElementById("valmin").value
  var temp12 = document.getElementById("valmax").value
  var temp13 = []
  for (var l = 0; l < resultofcounting.length; l++) {
    if (resultofcounting[l].mid >= temp11) {
      if (resultofcounting[l].mid <= temp12) {
        temp13.push(resultofcounting[l])
      }
    }
  }
  for (var h = 0; h < temp13.length; h++) {
    var tostringAgain = temp13[h].currency + " " +temp13[h].mid
    var list = document.getElementById("midlist")
    var node = document.createElement("LI");
    var button = document.createElement("BUTTON");
    button.value = temp13[h].code;
    button.innerHTML = tostringAgain
    node.appendChild(button);
    list.appendChild(node);
  }
}

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      ratesA: [],
      startDate: new Date(),
      endDate: new Date(),
      choosedCurrency: "1",
      choosedStartDate: "",
      choosedEndDate: "",
      choosedValMin:"",
      choosedValMax:"",
      pickresults: []
    };
    this.handleChangeStart = this.handleChangeStart.bind(this);
    this.handleChangeEnd = this.handleChangeEnd.bind(this);
  }

handleChangeStart(date) {
  var temp = date.toISOString().split("T")[0]
    this.setState({
      startDate: date,
      choosedStartDate: temp
    });
    
  }

handleChangeEnd(date) {
  var temp = date.toISOString().split("T")[0]
    this.setState({
      endDate: date,
      choosedEndDate: temp
    });
    
  }
handleValMin = () =>{
  var temp1 = document.getElementById("valmin").value
  this.setState({
    choosedValMin: temp1
  })
}
handleValMax = () =>{
  var temp1 = document.getElementById("valmax").value
  this.setState({
    choosedValMax: temp1
  })
}
  componentDidMount() {
    fetch("http://api.nbp.pl/api/exchangerates/tables/A/")
      .then(res => res.json())
      .then(result => {
        this.setState({
          ratesA: result[0].rates
        })
      })
      var temp1 = this.state.startDate.toISOString().split("T")[0]
      var temp2 = this.state.endDate.toISOString().split("T")[0]
      this.setState({
        choosedStartDate: temp1,
        choosedEndDate: temp2,
      });
}



  selectcurrency=()=>{
    var temp1 = this.state.choosedStartDate
    var temp2 = this.state.choosedEndDate
    if (temp1 < temp2) {
      var date_diff_indays = function(date1, date2) {
        var dt1 = new Date(date1);
        var dt2 = new Date(date2);
        return Math.floor((Date.UTC(dt2.getFullYear(), dt2.getMonth(), dt2.getDate()) - Date.UTC(dt1.getFullYear(), dt1.getMonth(), dt1.getDate()) ) /(1000 * 60 * 60 * 24));
      }
    var temp4 = date_diff_indays(temp1, temp2)
        if (temp4 >= "93") {
          var temp5 = new Date(temp2)
          var temp6 = Date.UTC(temp5.getFullYear(), temp5.getMonth(), temp5.getDate())
          
          

        }else if (temp4 < "93") {
          fetch("http://api.nbp.pl/api/exchangerates/tables/A/"+temp1+"/"+temp2+"/")
          .then(res => res.json())
          .then(result => {
            this.setState({
              pickresults: result
            })
            setTimeout(function() {
            findMidCur(result)

          }, 100);
          })
        }
      }else if (temp1 > temp2) {
        alert("Wybrana data startu jest większa od końcowej")
      }
  }

  handleCurChange = (e) =>{
    this.setState({ 
      choosedCurrency: e.target.value 
    })
  }

  test = () =>{
    console.log(this.state.choosedStartDate)
    console.log(this.state.choosedEndDate)
    console.log(this.state.choosedCurrency)
    console.log(this.state.choosedValMin)
    console.log(this.state.choosedValMax)
    console.log(this.state.pickresults)
  }
  render() {
    const { ratesA, pickresults } = this.state;
      return (
        <div id="main">
        <div id="setrow">
        {/*<div id="curselect">
        <span>Wybierz walutę</span>
        <select 
        id="currencyselect"
        value={this.state.choosedCurrency}
        onChange={this.handleCurChange}
        >
        <option value="1">Wybierz walutę</option>
          {ratesA.map(rate => (
            <option value={rate.code}>
              {rate.currency} {rate.code}
            </option>
          ))}
        </select>
        </div>*/}
        <div id="startdateselect">
        <span>Wybierz początkowy dzień</span>
        <DatePickerStart
        selected={this.state.startDate}
        onChange={this.handleChangeStart}
    />
    </div>
    <div id="enddateselect">
    <span>Wybierz dzień zakończenia</span>
    <DatePickerEnd
        selected={this.state.endDate}
        onChange={this.handleChangeEnd}
    />
    </div>
    <button onClick={this.selectcurrency}>
        Znajdź
        </button>
   <button onClick={this.test}>
        Test Me!
        </button>
        </div>
        <div>
        <div>
        <span>Wybierz kwotę minimalną</span>
        <input type="number" id="valmin" name="valmin"
       min="0" max="100" onChange={this.handleValMin}></input>
       </div>
       <div>
       <span>Wybierz kwotę maksymalną</span>
       <input type="number" id="valmax" name="valmax"
       min="1" max="100" step="0.01" onChange={this.handleValMax}></input>
       </div>  
        </div>
        <div>
        <div>
        <ul id="midlist">
        </ul>
        </div>
        <div>

        </div>
        </div>
        </div>
      );
  }
}

export default App;
